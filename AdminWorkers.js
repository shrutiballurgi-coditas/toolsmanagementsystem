import http from "./http.js"
const workerTable = document.getElementById("table");

function adminsWorker(workers,token) {
    const workerTemplate = `<button id="create-worker">Create</button>
    <table id="workertable">
    <thead>
    <th>Worker Id</th>
    <th>Worker Name</th>
    <th>Worker Username</th>
    <th>Worker Salary</th>
    <th>Worker order</th>
    </thead>
    <tbody id="worker-row">
    </tbody>
    </table>`;

    workerTable.innerHTML = workerTemplate;

    // const createTool=document.getElementById("create-tool");
    const createWorkerBtn=document.getElementById("create-worker");
    const workerRows = document.getElementById("worker-row");
    if (createWorkerBtn.addEventListener("click", () => {
        createWorker(token);
    }));
    
    let row=""
    for (let worker of workers) {
        const {workerId,workerName,workerUsername,workerSalary,worker_orders}=worker;
        console.log(workerId);
        row = row + workerRow(worker);
        workerRows.innerHTML=row;
    }
    const table=document.getElementById("workertable");
        table.addEventListener("click",(event)=>{
        const selectedClass=event.target.className;
        console.log(selectedClass);
        if(selectedClass==="edit"){
            updateWorker(event,workers,token);
        }
        else if(selectedClass==="delete"){
            removeWorker(event,token);

        }
    })
}


function createWorker(token){ 
    
        const updateSection=document.createElement("section");
        updateSection.classList.add("update_section")
        const createWorkerForm=`<form id="form">
        <h2>Create Worker</h2>
        
        <label for="workerName">Worker Name</label>
        <input type="text" id="workerName">
        <label for="workerUsername">Worker Username</label>
        <input type="text" id="workerUsername">
        <label for="workerPassword">Worker Id</label>
        <input type="text" id="workerPassword">
        <label for="workerSalary">worker Salary</label>
        <input type="number" id="workerSalary">
        <label for="workerOrder">worker Order</label>
        <input type="text" id="workerOrder">
        <button>Submit</button>
        </form>`;
        updateSection.innerHTML=createWorkerForm;
        document.body.append(updateSection);

        const createForm=document.getElementById("form");
        if (createForm.addEventListener("submit", async () => {
            
            let workerName = document.getElementById('workerName').value;
            let workerUsername = document.getElementById('workerUsername').value;
            let workerPassword = document.getElementById('workerPassword').value;
            let workerSalary= document.getElementById('workerSalary').value;
            let workerorder= "null"
            
            try {
                const data={
                        
                            "workerName": workerName,
                            "workerUsername": workerUsername,
                            "workerPassword": workerPassword,
                            "workerSalary": workerSalary,
                        
                
                        }
                const response=await http.post("admin/createWorker",token,data);
            }
            catch (e) {
                console.log(e)
            }
    
        }));


    }

function workerRow(worker) {
    const{workerId,workerName,workerUsername,workerSalary,worker_orders}=worker;
    return (`<tr>
            <td>${workerId}</td>
            <td>${workerName}</td>
            <td>${workerUsername}</td>
            <td>${workerSalary}</td>
            <td>${worker_orders}</td>
            <td>
                <button id="${workerId}" class="edit">Edit</button>
                <button id="${workerId}" class="delete">Delete</button>
            </td>
        </tr>`
    )
}


function updateWorker(event,workers,token) {
    const selectedClass=event.target.className;
    console.log(selectedClass);
    const id=event.target.id;
    console.log(id);
    for (let worker of workers){
        
        const { workerId, workerName, workerUsername, workerPassword,workerSalary, worker_orders } = worker;
        if (id===workerId){
        const updateSection = document.createElement("section");
        updateSection.classList.add("update_section");
        const updateForm = `<form id="form" class="form">
        <label for="workerId">Worker Id</label>
        <input type="number" id="workerId" value=${workerId}>
        <label for="workerName">Worker Name</label>
        <input type="text" id="workerName" value=${workerName}>
        <label for="workerUsername">Worker Username</label>
        <input type="text" id="workerUsername" value=${workerUsername}>
        <label for="workerPassword">Worker Password</label>
        <input type="text" id="workerPassword" value=${workerPassword}>
        <label for="workerSalary">Worker Salary</label>
        <input type="number" id="workerSalary" value=${workerSalary}>
        <label for="workerOrders">Worker Orders</label>
        <input type="number" id="workerOrders" value=${worker_orders}>
        <button>Submit</button>
        </form>`;
        updateSection.innerHTML=updateForm;
        document.body.append(updateSection);


        const updateWorkerForm = document.getElementById("form");
        if (updateWorkerForm.addEventListener("submit", async () => {
            let workerId = document.getElementById('workerId').value;
            let workerName = document.getElementById('workerName').value;
            let workerUsername = document.getElementById('workerUsername').value;
            let workerPassword = document.getElementById('workerPassword').value;
            let workerSalary = document.getElementById('workerSalary').value;
            let workerOrders = document.getElementById('workerOrders').value;

            try {
                
                const updatedData={
                    
                            "workerId": workerId,
                            "workerName": workerName,
                            "workerUsername": workerUsername,
                            "workerSalary": workerSalary,
                            "workerPassword":workerPassword,
                                
                                
                    
                }
                const updateWorker=await http.put("admin/updateWorker",updatedData,token)
            }
            catch (e) {
                console.log(e)
            }

        }));
}
}
}


function removeWorker(event,token){

    const selectedClass=event.target.className;
    console.log(selectedClass);
    const id=event.target.id;
    console.log(id);
    
    const deleteSection = document.createElement("section");
    const deletetemplate=`<div class="delete-container" id="delete-container">
    <p>Are you sure to delete worker</p>
    <button class="button" id="yesBtn">Yes</button>
    <button class="button" id="noBtn">No</button>

    </div>`
    deleteSection.innerHTML=deletetemplate;
    document.body.append(deleteSection);
    const yesButton=document.getElementById("yesBtn");
    const noButton=document.getElementById("noBtn");
    const div=document.getElementById("delete-container");
   


    yesButton.addEventListener("click",async()=>{
    
    try {
        //  fetch(`http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io/admin/deleteWorker/${id}`, {
        //     method: "DELETE",
        //     headers:{
        //         "Authorization":"Bearer "+token,
        //         "ngrok-skip-browser-warning": "1234",
        //         'Content-type': 'application/json; charset=UTF-8'
        //     }

        // });
        const deleteWorker=await http.delete(`admin/deleteWorker/${id}`,token);
    } catch (err) {
        console.log(err);
    }

    noButton.addEventListener("click",()=>{
        div.style.display="none";
    }
    )
})
}



const getWorkers = async (token) => {
    try {
        // const response = await fetch("http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io/admin/getWorkers", {

        //     method: "GET",
        //     headers: new Headers({
        //         "Authorization": "Bearer " + token,
        //         "ngrok-skip-browser-warning": "1234",
        //         'Content-type': 'application/json',

        //     })

        // });
        // const data = await response.json();
        // console.log(data);
        const workersData=await http.get("admin/getWorkers",token)
        adminsWorker(data, token);
        // return data;


    } catch (e) {
        console.log(e);
    }
}




export default { getWorkers }

