import http from "./http";

// const adminTools=document.getElementById("admin-tools")
const toolsTable=document.getElementById("table");
function adminsTools(tools,token){
    const toolTemplate=`<button id="create-tool">Create</button>
    <table>
    <thead>
    <th>Tool Id</th>
    <th>Tool Name</th>
    <th>Tool Size</th>
    <th>Tool price</th>
    </thead>
    <tbody id="tool-row">
    </tbody>
    </table>`;

    toolsTable.innerHTML=toolTemplate;

    
    const toolRows=document.getElementById("tool-row");

    let toolsRow="";
    
    let row=""  
    for(let tool of tools){
               console.log(tool)
                row=row + toolRow(tool);
                toolRows.innerHTML=row;
    
                const editTool=document.getElementById("edit-tool");
                console.log(editTool);
                
                const deleteTools=document.getElementById("delete-tool");
                

                
                editTool.addEventListener("click",()=>{
                    updateTool(tool,token);
                })
                deleteTools.addEventListener("click",()=>{
                    removeTool(tool.toolId,token);
                });
                
               
    }
    
    console.log("hi");

    const createTool=document.getElementById("create-tool");
    
    if(createTool.addEventListener("click",async ()=>{
        const updateSection=document.createElement("section");
        const createToolForm=`<form id="form">
        <h2>Create Tool</h2>
        <label for="toolId">Tool Id</label>
        <input type="number" id="toolId">
        <label for="toolName">Tool Name</label>
        <input type="text" id="toolName">
        <label for="toolSize">Tool Size</label>
        <input type="number" id="toolSize">
        <label for="toolPrice">Tool Price</label>
        <input type="number" id="toolPrice">
        <button>Submit</button>
        </form>`;
        updateSection.innerHTML=createToolForm;
        document.body.append(updateSection);

        const createForm=document.getElementById("form");
        if (createForm.addEventListener("submit", async () => {
            let toolId = document.getElementById('toolId').value;
            let toolName = document.getElementById('toolName').value;
            let toolSize = document.getElementById('toolSize').value;
            let toolPrice = document.getElementById('toolPrice').value;
            
            try {
                const responseData=await post("admin/postTool",data,token)
            }
            catch (e) {
                console.log(e)
            }
    
        }));


    }));
}


function toolRow(tool){
    const {toolId,toolName,toolSize,toolPrice}=tool;
    let templateRow=`<tr>
            <td>${toolId}</td>
            <td>${toolName}</td>
            <td>${toolSize}</td>
            <td>${toolPrice}</td>
            <td>
                <button id="edit-tool">Edit</button>
                <button id="delete-tool">Delete</button>
            </td>
        </tr>`;
    return(templateRow);

    

    
}








function updateTool(tool,token){
    const {toolId,toolName,toolSize,toolPrice}=tool;
    const updateSection=document.createElement("section");
    const updateForm=`<form id="form">
    <label for="toolId">Tool Id</label>
    <input type="number" id="toolId" value=${toolId}>
    <label for="toolName">Tool Name</label>
    <input type="text" id="toolName" value=${toolName}>
    <label for="toolSize">Tool Size</label>
    <input type="number" id="toolSize" value=${toolSize}>
    <label for="toolPrice">Tool Price</label>
    <input type="number" id="toolPrice" value=${toolPrice}>
    <button>Submit</button>
    </form>`;
    updateSection.innerHTML=updateForm;
    document.body.append(updateSection);


    const updateToolForm=document.getElementById("form");
    if (updateToolForm.addEventListener("submit", async () => {
        let toolId = document.getElementById('toolId').value;
        let toolName = document.getElementById('toolName').value;
        let toolSize = document.getElementById('toolSize').value;
        let toolPrice = document.getElementById('toolPrice').value;
        
        try {
            
            body={
                        "toolId": toolId,
                        "toolName": toolName,
                        "toolSize": toolSize,
                        "toolPrice": toolPrice,
                    }

            const updateToolData=await http("admin/updateTool" ,data,token)
        }
        catch (e) {
            console.log(e)
        }

    }));
}


function removeTool(id,token){
    console.log(id);
    const div=document.createElement("div");
    div.classList.add("delete-container");
    const alertStatement=document.createElement("p");
    document.body.append(div);
    alertStatement.innerText="Are you Sure to delete tool?";
    div.appendChild(alertStatement);
    const yesButton=document.createElement("button");
    yesButton.innerText="Yes";
    yesButton.classList.add("yesButton");
    div.appendChild(yesButton);
    const noButton=document.createElement("button");
    noButton.innerText="No";
    noButton.classList.add("noButton");
    div.appendChild(noButton);
    console.log(id);
    yesButton.addEventListener("click",async ()=>{
    
    try {
        // 
        const deleteToolData= await delete(`admin/deleteTool/${id}`,token);
    } catch (err) {
        console.log(err);
    }

    noButton.addEventListener("click",()=>{
        div.style.display="none";
    }
    )
})
}




const getTools = async (token) => {
    try {
        const response = await fetch("http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io/admin/getTools", {

            method: "GET",
            headers: new Headers({
                "Authorization":"Bearer "+token,
                "ngrok-skip-browser-warning": "1234",
                'Content-type': 'application/json',
                
            })

        });
        const data = await response.json();
        console.log(data);
        adminsTools(data,token);
        // return data;


    } catch (e) {
        console.log(e);
    }
}

export default{getTools}