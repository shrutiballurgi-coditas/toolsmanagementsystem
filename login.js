import adminPage from "./adminDashboard.js";

const loginForm=document.getElementById("form-data");
loginForm.addEventListener("submit", async (e)=>{
    e.preventDefault();
    let userName=document.getElementById("username").value;
    let password=document.getElementById("password").value;

    try{
        const loginData= {
            "username":userName,
            "password":password,
        },
        const response=  await post("authenticate",token)
        
        }
        const data = await response.json();
        console.log(data);
        const token=data.jwtToken;
        console.log(token);
        const role=data.role;
        console.log(role);
        const id=data.id;
        console.log(id);
        if(role==="ADMIN"){
            adminPage['adminPage'](token);
        }
        
    }
    catch(e){
        console.log(e);
    }
    loginForm.style.display="none";
})




