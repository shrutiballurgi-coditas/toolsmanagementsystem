import getOrders from "./adminOrders.js";
import getTools from "./adminTools.js";
import getWorkers from "./AdminWorkers.js";

const adminDashboard=document.getElementById("admin-dashboard");

function adminPage(token){
    
    const dashboardTemplate=`<section class="tool-bar" id="tool-bar">
    <section id="admin-workers">Workers</section>
    <section id="admin-orders">Orders</section>
    <section id="admin-tools">Tools</section>
    </section>
    <section class="details-area"></section>`;
    
    adminDashboard.innerHTML=dashboardTemplate;
    
    const adminWorkers=document.getElementById("admin-workers");
    adminWorkers.style.display="block";
    console.log(adminWorkers);
    
    const adminOrders=document.getElementById("admin-orders");
    adminOrders.style.display="block";
    console.log(adminOrders);
    const adminTools=document.getElementById("admin-tools");
    adminTools.style.display="block";
    console.log(adminTools);

    if(adminOrders.addEventListener("click",()=>{
        console.log("yjfthdr")
        console.log(token);
        getOrders['getOrders'](token);
    }));

    else if(adminTools.addEventListener("click",()=>{
        getTools['getTools'](token);
    }));

    else if(adminWorkers.addEventListener("click",()=>{
        getWorkers['getWorkers'](token);
    }));
}




export default {adminPage}