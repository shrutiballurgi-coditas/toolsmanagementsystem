import http from "./http.js"
function adminsOrders(ordersList){
    
    // const orders=document.getElementById("admin-orders");
    // console.log(orders)
    const table=document.getElementById("table");

    let ordersTemplate=`<table class="ordersTable">
    <thead>
    <th>Order Id</th>
    <th>Customer Name</th>
    <th>Worker Id</th>
    <th>Worker Name</th>
    <th>ordered Tools</th>
    <th>order Status</th>
    <th>price</th>
    </thead>
    <tbody id="order-row">
    </tbody>

    </table>`;
    table.innerHTML=ordersTemplate;
    const orderRows=document.getElementById("order-row");
    console.log(orderRows);
    let ordersRow=``;
    // fetch stmt needed
    // const ordersList = getOrders();
    // console.log(orders);
    for (let order of ordersList){
        
        const {orderId, customerName,workerId,workerName,orderStatus,price,worker_orders}=order;
        console.log("hi");
        console.log(worker_orders['orderId'])
        ordersRow =ordersRow+`<tr>
        <td>${worker_orders.orderId}</td>
        <td>${customerName}</td>
        <td>${workerId}</td>
        <td>${workerName}</td>
        <td>${orderStatus}</td>
        <td>${price}</td>

        </tr>`;

        orderRows.innerHTML=ordersRow;

    }
        
    
}


const getOrders = async (token) => {
    try {
        

        const response=await http.get("admin/getOrders");
        adminsOrders(response);
    


    } 
    catch(e){
        console.log(e);
    }
    

        
        

    
        

}


export default {getOrders}