class Http {
    #baseUrl = "http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io";

    async send(endpoint,token, options = {}, data = null) {
        try {
            options = {
                ...options,
                headers: {
                    Authorization: `Bearer ${token}`,
                    "ngrok-skip-browser-warning": "1234",
                    'Content-Type': 'application/json'
                },
                body: data ? JSON.stringify(data) : null
            }

            const response = await fetch(`${this.#baseUrl}/${endpoint}`, options);
            const responseData = await response.json();

            return responseData;
        } catch (e) {
            throw e;
        }
    }

    async get(endpoint,token) {
        return await this.send(endpoint);
    }

    async delete(endpoint,token) {
        return await this.send(endpoint, { method: 'DELETE' });
    }

    async post(endpoint, data,token) {
        return await this.send(endpoint, { method: 'POST' }, data)
    }

    async put(endpoint, data,token) {
        return await this.send(endpoint, { method: 'PUT' }, data)
    }
}

const http = new Http();

export default http;
